const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

const logger = console;

function UserRouter(params) {

  router.get('/', getAllUsers);
  router.post('/register', upload.array('files', 12), registerUser);
  router.get('/:userId', getUserById);
  router.delete('/:userId', deleteUser);

  return router;
}

function getAllUsers(req, res) {

  res.json({
    method: `${req.method}`,
    baseUrl: `${req.baseUrl}`,
    serverTime: new Date()
  });
}

// multer -> file upload module
function registerUser(req, res) {

  let reqBody = req.body;
  logger.info(`Request Body: ${JSON.stringify(reqBody)}`);

  res.json({
    method: `${req.method}`,
    baseUrl: `${req.baseUrl}`,
    serverTime: new Date()
  });
}

function getUserById(req, res) {
  res.json({
    method: `${req.method}`,
    baseUrl: `${req.baseUrl}`,
    serverTime: new Date()
  });
}

function deleteUser(req, res) {
  res.json({
    method: `${req.method}`,
    baseUrl: `${req.baseUrl}`,
    serverTime: new Date()
  });
}

module.exports = UserRouter;