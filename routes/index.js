const express = require('express');
const router = express.Router();

const userRoute = require('./user');

function DefaultRouter() {

  router.get('/', (req, res) => {
    return res.render('home', {
      title: 'Eugene J Rautenbach'
    });
  });

  router.post('/', (req, res) => {
    res.json({
      message: 'Hello world!',
      method: 'Post',
      serverTime: new Date()
    });
  });

  return router;
}

module.exports = (app) => {

  app.use('/', DefaultRouter());
  app.use('/users', userRoute());
}
