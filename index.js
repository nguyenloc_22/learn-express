const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes');

const path = require('path');

const logger = console;
const Port = 3000;

let app = null;

startApp();

function startApp() {
    logger.info('Start application.');

    app = express();

    app.set('view engine', 'ejs');
    app.set('views', path.join(__dirname, './views'));

    app.use(cors());
    app.use(express.static(path.join(__dirname, './public')));
    app.get('/favicon.ico', (req, res) => res.sendStatus(204));

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    routes(app);

    app.listen(Port, (err) => {
        if(err) throw err;
        
        logger.info(`Web server is running at port ${Port}`);
    });
    
}


