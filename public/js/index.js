(function () {

  var logger = console;
  var files;

  var contextRoot = "/" + window.location.pathname.split('/')[1];

  runApp();

  function runApp() {
    logger.info('Start applicaiton');
    bindEvents();
  }

  function bindEvents() {
    $('#files').on('change', prepareUpload);
    $('#form-upload').bind('submit', handleFormUpload);
  }

  function prepareUpload(event) {
    files = event.target.files;

    if(files === null || files === undefined) {
      return;
    }

    // for(var i = 0; i < files.length; i++) {
    //   console.log(files[i]);
    // }
  }

  function handleFormUpload(event) {
    // event.stopPropagation();
    event.preventDefault();

    var d = serializeFormData($(event.target));

    var data = new FormData();
    $.each(files, (key, file) => {
      data.append('files', file);
      console.log(`Prepare: ${key}`);
    });

    $.each(d, (key, value) => {
      data.append(key, value);
      console.log(`Prepare: ${key}`);
    });

    logger.info(d);

    $.ajax({
      url: contextRoot + 'users/register',
      method: 'post',
      timeout: 0,
      processData: false,
      mineType: 'multipart/form-data',
      contentType: false,
      data: data,
      success: handleSuccess,
      error: handleError
    });
  }

  function handleSuccess(resp) {
    logger.info(`Post successfully: ${JSON.stringify(resp)}`);
  }

  function handleError(err) {
    logger.error(`Failed to register.`, err);
  }

  function serializeFormData(form) {
    var jsonObject = {};
    var array = form.serializeArray();
    $.each(array, function () {
      jsonObject[this.name] = this.value;
    });

    return jsonObject;
  }

})();
